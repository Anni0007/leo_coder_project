create database leo_coder;
use leo_coder;

create table users (id integer primary key auto_increment, name varchar(50),
email varchar(50) unique, password varchar(200));