const express = require('express')
const app = express()
const bodyParser = require('body-parser')


app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

const userRouter = require('./routes/user')

//add routes
app.use(userRouter)

// homepage
app.get('/', (request,response)=>{
    console.log('homepage')
    response.send("homepage")
})



app.listen('4000','0.0.0.0', ()=> {
 console.log("welcome to my app")
})

