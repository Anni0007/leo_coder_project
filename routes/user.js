const express = require('express')
const db = require('../db')
const crypto = require('crypto-js')
const utils = require('../utils')

const router = express.Router()


// User Registration Api 

router.post('/userRegistration', (request,response)=>{
    // destructuring  request body 
    const { name , email, password} = request.body

  // encrypt the password
  const encryptedPassword = '' + crypto.SHA256(password)

  const statement = `insert into users (name,  email, password) values 
                    ('${name}', '${email}', '${encryptedPassword}')`

  db.execute(statement, (error,data)=>{
      response.send(utils.createResult(error, data))
  })
})

// Get all User 
router.get('/alluser',(request,response)=>{

    const statement = `select name, email from users;`
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error, data))
    })
})


// Get User  by id
router.get('/user/:id',(request,response)=>{

    const {id} = request.params

    const statement = `select name, email from users where id = ${id} ;`
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error, data))
    })
})

// Update User  Name 
router.patch('/user/:id',(request,response)=>{

    const {id} = request.params
    const{name} = request.body

    const statement =  `update users set name = '${name}'where id = '${id}';`
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error, data))
    })
})

//delete user by id 
router.delete('/user/:id',(request,response)=>{
    const { id } = request.params
    const statement =  `delete from users where id = '${id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//delete user by email
router.delete('/user',(request,response)=>{
    const { email} = request.body
    const statement =  `delete from users where email = '${email}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

 module.exports = router